import request from "supertest";
import { describe, expect, test } from "@jest/globals";
import { app } from "../../../src/index";
import { User } from "../../app/models/Users.model";
import prisma from "../../../prismaclient";

const routerRegister = "/auth/register";
const routerUserEdit = "/auth/user";
let userCreated: User;
const user = {
	email: "johndoe@example.com",
	name: "John Doe",
	nif: "69395628006",
	birthdate: "01/02/2003",
	password: "123456o",
};
const AuthorizationBearerObject = { name: "Authorization", value: "" };

describe("Criação de usuário", () => {
	test("Deve cadastrar um novo usuário", async () => {


		const response = await request(app)
			.post(routerRegister)
			.send(user);

		userCreated = response.body.data;
		console.log(userCreated);
		expect(response.status).toBe(201);

	});

	test("Gerar erro ao tentar criar usuário faltando o campo nome", async () => {

		const userCopy = { ...user };
		userCopy.name = "";
		const response = await request(app)
			.post(routerRegister)
			.send(userCopy);
		expect(response.status).toBe(400);
		expect(response.body.message).toBe("O campo nome é obrigatório");

	});

	test("Deve gerar erro se o CPF enviado for inválido", async () => {
		const userCopy = { ...user };
		userCopy.nif = "12345678901";

		const response = await request(app)
			.post(routerRegister)
			.send(userCopy);
		expect(response.status).toBe(400);
		expect(response.body.message).toBe("CPF inválido");
	});

	test("Deve gerar erro se o CNPJ enviado for inválido", async () => {
		const userCopy = { ...user };
		userCopy.nif = "12345678901234";

		const response = await request(app)
			.post(routerRegister)
			.send(userCopy);
		expect(response.status).toBe(400);
		expect(response.body.message).toBe("CNPJ inválido");
	});

	test("Deve gerar erro, pois o usuário já existe no sistema", async () => {
		const response = await request(app)
			.post(routerRegister)
			.send(user);
		expect(response.status).toBe(400);
		expect(response.body.message).toBe("Usuário já cadastrado no sistema!");
	});

	test("Deve gerar erro, pois o email enviado é inválido", async () => {
		const userEmailInvalid = { ...user };
		userEmailInvalid.email = "emailinvalid.com";

		const response = await request(app)
			.post(routerRegister)
			.send(userEmailInvalid);

		expect(response.status).toBe(400);
		expect(response.body.message).toBe("E-mail inválido!");
	});

});

describe("Edição do usuário", () => {
	const editUser = {
		name: "John Doe Edited",
		birthdate: "20/08/2004"
	};
	beforeAll(async () => {
		//Executa login de usuário e pega o token para ser enviado na requisição

		const credentialsLogin = { email: user.email, password: user.password };

		const responseLogin = await request(app)
			.post("/auth/login")
			.send(credentialsLogin);

		AuthorizationBearerObject.value = `Bearer ${responseLogin.body.data.token}`;

	});


	test("Edição do usuário com sucesso", async () => {
	
		const response = await request(app)
			.put(`${routerUserEdit}/${userCreated.id}`)
			.send(editUser)
			.set(AuthorizationBearerObject.name, AuthorizationBearerObject.value);

		expect(response.status).toBe(200);
		expect(response.body.message).toBe("Usuário editado com sucesso!");

	});

	test("Se caso o usuário não existir no sistema", async () => {
		const editUser = {
			name: "John Doe Edited",
			birthdate: "20/08/2004"
		};
		const response = await request(app)
			.put(`${routerUserEdit}/0`)
			.send(editUser)
			.set(AuthorizationBearerObject.name, AuthorizationBearerObject.value);

		expect(response.status).toBe(404);
		expect(response.body.message).toBe("Usuário não encontrado!");
	});

	test("Se o objeto para edição for vazio", async() => {
		editUser.name = "";
		editUser.birthdate = "";

		const response = await request(app)
			.put(`${routerUserEdit}/0`)
			.send(editUser)
			.set(AuthorizationBearerObject.name, AuthorizationBearerObject.value);

		expect(response.status).toBe(400);
		expect(response.body.message).toBe("É necessário os dados que deseja alterar!");
	});
});

afterAll(async () => {
	try {
		const deletedUser = await prisma.user.delete({
			where: {
				nif: userCreated.nif
			}
		});
		console.log("Usuário deletado:", deletedUser);
	} catch (error) {
		console.error("Erro ao deletar usuário:", error);
	} finally {
		await prisma.$disconnect();
	}
});


