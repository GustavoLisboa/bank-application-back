import request from "supertest";
import { describe, expect, test } from "@jest/globals";
import { app } from "../../../src/index";
import { VerifyTokenReceived } from "../../app/utils/tokenAuth";


describe("Teste de Login do Usuário", () => {
	const authLoginRoute = "/auth/login";
	const token_valid = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImVtYWlsIjoiam9uaGRvZUB0ZXN0ZS5jb20iLCJwYXNzd29yZCI6IjEyMzQ1NjdvIn0sImlhdCI6MTY4NTA3MDM0N30.4V2TOvWcGHskk0xhigpv-zzt0y-KQmcISy7u1yxU1Cg";
	const token_invalid = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo1LCJjcmVhdGVkX2F0IjoiMjAyMy0wNS0yNFQyMjozMDo0Ni4yMDNaIiwiZW1haWwiOiJyb3NlbXlhbHZlc0B0ZXN0ZS5jb20iLCJuYW1lIjoiUm9zZW15IEFsdmVzIExpc2JvYSIsIm5pZiI6IjE4NTE4MTU0ODEyIiwiYmlydGhkYXRlIjoiMTYvMDYvMTk3NCIsInR5cGVfdXNlciI6MH0sImlhdCI6MTY4NDk4MDE5OCwiZXhwIjoxNjg1MDQ0OTk4fQ.JXOQppF3Z1RjGWMZkFgRgaW7fIi-V8wmteDZMiJKhg0";
	const login_user_valid = { email: "jonhdoe@teste.com", password: "1234567o" };

	test("deve retornar status 200 e um objeto se o usuário for logado com sucesso ", async () => {
		const loginUser = { email: "jonhdoe@teste.com", password: "1234567o" };

		const response = await request(app)
			.post(authLoginRoute)
			.send(loginUser);

		expect(response.status).toBe(200);
		expect(response.body.success).toBe(true);
		expect(response.body.data).toBeDefined();
		expect(response.body.data.token).toBeDefined();
		expect(response.body.data.user).toBeDefined();
	});

	test("Se o servidor receber sem email, deve retornar erro 400 e uma message", async () => {
		const loginUser = { email: "", password: "1234567o" };

		const response = await request(app)
			.post(authLoginRoute)
			.send(loginUser);

		expect(response.status).toBe(400);
		expect(response.body.success).toBe(false);
		expect(response.body.message).toBe("É necessário email para realizar login!");
	});

	test("Se o servidor receber sem senha, deve retornar erro 400 e uma message", async () => {
		const loginUser = { email: "jonhdoe@teste.com", password: "" };

		const response = await request(app)
			.post(authLoginRoute)
			.send(loginUser);

		expect(response.status).toBe(400);
		expect(response.body.success).toBe(false);
		expect(response.body.message).toBe("É necessário senha para realizar login!");
	});

	test("Se caso as credenciais do usuário forem incorretas", async () => {
		const loginUserInvalid = { email: "jonhdoe@teste.com", password: "123456" };


		const response = await request(app)
			.post(authLoginRoute)
			.send(loginUserInvalid);

		expect(response.status).toBe(401);
		expect(response.body.message).toBe("Credenciais inválidas, verifique seu email e senha!");
	});

	test("Se caso o usuário não existir no banco de dados", async () => {
		const loginNotExist = { email: "usernotExist@teste.com", password: "testNotExist" };

		const response = await request(app)
			.post(authLoginRoute)
			.send(loginNotExist);

		expect(response.status).toBe(404);
		expect(response.body.message).toBe("Usuário não encontrado!");
	});

	test("Deve retornar o conteúdo decodificado do token", () => {
		const decoded = VerifyTokenReceived(token_valid);
		let data;

		if (typeof decoded !== "string") {
			data = decoded.data;
		}
		expect(data).toEqual(login_user_valid);
	});

	test("Deve lançar uma exceção ao fornecer um token inválido", () => {
		expect(() => {
			VerifyTokenReceived(token_invalid);
		}).toThrow();
	});

});

describe("Teste de Logout Usuário", () => {
	let token_gerado: string;
	const authLoginRoute = "/auth/login";
	const authLogoutRoute = "/auth/logout";
	const loginUser = { email: "jonhdoe@teste.com", password: "1234567o" };
	const AuthorizationBearerObject = { name: "Authorization", value: "" };

	beforeAll(async () => {
		//Realiza o login e armazena na variavel de token.
		const response = await request(app)
			.post(authLoginRoute)
			.send(loginUser);

		token_gerado = response.body.data.token;
		AuthorizationBearerObject.value = `Bearer ${token_gerado}`;
	});

	test("Logout gerado com sucesso!", async () => {
		const responseLogout = await request(app)
			.post(authLogoutRoute)
			.set(AuthorizationBearerObject.name, AuthorizationBearerObject.value);

		expect(responseLogout.status).toBe(200);
		expect(responseLogout.body.message).toBe("Logout realizado com sucesso!");
	});

	test("Logout com Bearer Token que já foi feito logout anteriormente", async () => {
		const responseLogout = await request(app)
			.post(authLogoutRoute)
			.set(AuthorizationBearerObject.name, AuthorizationBearerObject.value);

		expect(responseLogout.status).toBe(400);
		expect(responseLogout.body.message);
	});
});

