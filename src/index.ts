import express from "express";
import { PrismaClient } from "@prisma/client";
import { router } from "./app/routes";

export const app = express();
const port = 3000;
app.use(express.json());
app.use(router);

app.listen(port, () => {
	console.log(`Server started at http://localhost:${port}`);
});

