import jwt from "jsonwebtoken";
import { User } from "../models/Users.model";
export const SECRET = "4sRsWLgDgKtkk4WNsqtxb7zR7EYez2";


export async function TokenGenerate(data: unknown) {
	const token = jwt.sign(
		{ data },
		SECRET,
	);

	return token;
}

export function VerifyTokenReceived(token: any) {
	return jwt.verify(token, SECRET);
}