import { MessageReturnValid } from "../models/MessageReturnValid.model";

export function emailValidate(email: string): MessageReturnValid {
	const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
	const messageReturn: MessageReturnValid = { message: "", valid: false };
	if (regex.test(email) == true) {
		messageReturn.valid = true;
	} else {
		messageReturn.valid = false;
		messageReturn.message = "E-mail inválido!";
	}
	return messageReturn;
}