import { MessageReturnValid } from "../models/MessageReturnValid.model";


export function validateNif(nif: string): MessageReturnValid {
	const messageReturn: MessageReturnValid = { message: "", valid: false };
	if (nif.length == 11) {
		const valid: boolean = validateCpf(nif);
		if (valid == true) {
			messageReturn.message = "CPF válido";
			messageReturn.valid = valid;
		} else {
			messageReturn.message = "CPF inválido";
			messageReturn.valid = valid;
		}
	}

	if (nif.length == 14) {
		const valid: boolean = validateCnpj(nif);
		if (valid == true) {
			messageReturn.message = "CNPJ válido";
			messageReturn.valid = valid;
		} else {
			messageReturn.message = "CNPJ inválido";
			messageReturn.valid = valid;
		}
	}

	if (nif.length > 14 || nif.length < 11) {
		messageReturn.message = "O CPF ou CPNJ digitado não possuí carácteres suficientes.";
		messageReturn.valid = false;
	}
	return messageReturn;

}

function validateCpf(nif: string): boolean {
	const digits = nif.split("").map(Number);
	if (digits.every(digit => digit === digits[0])) {
		return false;
	}

	// Validate CPF algorithm
	let sum = 0;
	let mod;
	for (let i = 1; i <= 9; i++) {
		sum = sum + digits[i - 1] * (11 - i);
	}
	mod = (sum * 10) % 11;
	if (mod === 10 || mod === 11) {
		mod = 0;
	}
	if (mod !== digits[9]) {
		return false;
	}

	sum = 0;
	for (let i = 1; i <= 10; i++) {
		sum = sum + digits[i - 1] * (12 - i);
	}
	mod = (sum * 10) % 11;
	if (mod === 10 || mod === 11) {
		mod = 0;
	}
	if (mod !== digits[10]) {
		return false;
	}

	return true;
}

function validateCnpj(nif: string): boolean {
	const digits = nif.split("").map(Number);
	if (digits.every(digit => digit === digits[0])) {
		return false;
	}

	// Validate CNPJ algorithm
	let sum = 0;
	let mod;
	let factor = 5;
	for (let i = 0; i < 12; i++) {
		sum = sum + digits[i] * factor;
		factor = factor === 2 ? 9 : factor - 1;
	}
	mod = sum % 11;
	if (mod < 2) {
		if (digits[12] !== 0) {
			return false;
		}
	} else {
		if (digits[12] !== 11 - mod) {
			return false;
		}
	}

	sum = 0;
	factor = 6;
	for (let i = 0; i < 13; i++) {
		sum = sum + digits[i] * factor;
		factor = factor === 2 ? 9 : factor - 1;
	}
	mod = sum % 11;
	if (mod < 2) {
		if (digits[13] !== 0) {
			return false;
		}
	} else {
		if (digits[13] !== 11 - mod) {
			return false;
		}
	}

	return true;

}