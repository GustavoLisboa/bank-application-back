export function returnResponse(success: boolean, data: unknown, message: string) {
	return {
		success: success,
		message: message,
		data: data
	};
}