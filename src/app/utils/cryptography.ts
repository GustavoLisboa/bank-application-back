import bcrypt from "bcryptjs";

const saltRounds = 10;
export function cryptography(data: string) {
	return bcrypt.hashSync(data, saltRounds);
}

export function decryption(data_req: string, data: string) {
	const passwordMatch = bcrypt.compareSync(data_req, data);
	if (!passwordMatch) {
		return false;
	} else {
		return true;
	}
}