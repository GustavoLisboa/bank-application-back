import { Request, Response } from "express";
import { returnResponse } from "../utils/response";
import { User, UserEdit } from "../models/Users.model";
import { MessageReturnValid } from "../models/MessageReturnValid.model";
import { emailValidate } from "../utils/validationEmail";
import { validateNif } from "../utils/validationNif";
import { cryptography, decryption } from "../utils/cryptography";
import { LoginUser } from "../models/LoginUser.model";
import { MessageReturnUserJson } from "../models/MessageReturnUser";
import { TokenGenerate } from "../utils/tokenAuth";
import { getHeaderAuthorization } from "../utils/getHeaderAuthorization";
import { setRedis } from "../utils/redisClient";
import prisma from "../../../prismaclient";

async function registerUser(req: Request, res: Response) {
	const user: User = req.body;

	// Verifica se CPF/CNPJ é válido
	const nifValidation: MessageReturnValid = validateNif(user.nif);

	// Verifica se usuário existe no banco de dados
	const userExist = await prisma.user.findFirst({
		where: {
			nif: user.nif,
			email: user.email
		}
	});

	//verifica se todos campos estão preenchidos
	const fieldsValidation: MessageReturnValid = validateFields(user);

	//verifica se o email é válido
	const emailValidation: MessageReturnValid = emailValidate(user.email);

	if (!fieldsValidation.valid) {
		return res.status(400).json(returnResponse(false, {}, fieldsValidation.message));
	}

	if (!nifValidation.valid) {
		return res.status(400).json(returnResponse(false, {}, nifValidation.message));
	}

	if (userExist) {
		return res.status(400).json(returnResponse(false, {}, "Usuário já cadastrado no sistema!"));
	}

	if (!emailValidation.valid) {
		return res.status(400).json(returnResponse(false, {}, emailValidation.message));
	}

	try {
		const hashedPassword = cryptography(user.password);
		user.password = hashedPassword;
		const userCreated = await createUser(user);
		return res.status(201).json(returnResponse(true, userCreated, "Usuário criado com sucesso!"));
	} catch (error) {
		//console.log(error);
		return res.status(500).json(returnResponse(false, null, "Erro ao criar usuário!"));
	}
}

function validateFields(user: User): MessageReturnValid {
	const messageReturn: MessageReturnValid = { message: "", valid: true };
	const keys = Object.keys(user);

	for (const key of keys) {
		if (key in user) {
			switch (key) {
			case "name":
				if (!user.name) {
					messageReturn.message = "O campo nome é obrigatório";
					messageReturn.valid = false;
				}
				break;
			case "email":
				if (!user.email) {
					messageReturn.message = "O campo email é obrigatório";
					messageReturn.valid = false;
				}
				break;
			case "nif":
				if (!user.nif) {
					messageReturn.message = "O campo CPF/CNPJ é obrigatório";
					messageReturn.valid = false;
				}
				break;
			case "birthdate":
				if (!user.birthdate) {
					messageReturn.message = "O campo data de nascimento é obrigatório";
					messageReturn.valid = false;
				}
				break;
			case "password":
				if (!user.password) {
					messageReturn.message = "O campo senha é obrigatório";
					messageReturn.valid = false;
				}
				break;
			default:
				break;
			}
		}
	}

	return messageReturn;
}

async function createUser(user: User) {
	const newUserCreate = await prisma.user.create({
		data: user,
		select: {
			name: true,
			email: true,
			type_user: true,
			nif: true,
			birthdate: true,
			created_at: true,
			id: true
		}
	});
	return newUserCreate;
}

async function loginUser(req: Request, res: Response) {
	const credentials: LoginUser = req.body;

	if (!credentials.email) {
		return res.status(400).json(returnResponse(false, {}, "É necessário email para realizar login!"));
	}
	if (!credentials.password) {
		return res.status(400).json(returnResponse(false, {}, "É necessário senha para realizar login!"));
	}

	if (credentials.email && credentials.password) {
		try {
			const result: MessageReturnUserJson = await verifyCredentials(credentials);
			const validCredentials: MessageReturnUserJson = result;
			if (validCredentials.user !== null) {
				const { password, ...modifiedUser } = validCredentials.user;
				const token = await TokenGenerate(credentials);
				const user = modifiedUser;
				return res.status(200).json(returnResponse(true, { token, user }, "Usuário logado com sucesso!"));
			} else {
				return res.status(validCredentials.status).json(returnResponse(false, {}, validCredentials.message));
			}
		} catch (error) {
			return res.status(500).json(returnResponse(false, {}, "Erro interno no servidor!"));
		}
	}


}

async function verifyCredentials(credentials: LoginUser): Promise<MessageReturnUserJson> {
	const messageResponse: MessageReturnUserJson = { message: "", valid: false, user: null, status: 0 };
	const user: User | null = await prisma.user.findFirst({
		where: {
			email: credentials.email
		},
	});

	if (user === null) {
		messageResponse.message = "Usuário não encontrado!";
		messageResponse.valid = false;
		messageResponse.user = null;
		messageResponse.status = 404;
	} else {
		if (decryption(credentials.password, user.password) == false) {
			messageResponse.message = "Credenciais inválidas, verifique seu email e senha!";
			messageResponse.valid = false;
			messageResponse.user = null;
			messageResponse.status = 401;

		} else {
			messageResponse.valid = true;
			messageResponse.user = user;
		}
	}

	return messageResponse;
}

async function logoutUser(req: Request, res: Response) {
	const token = getHeaderAuthorization(req);
	try {
		const setTokenInvalid = await setRedis(`token:${token}`, "invalid");
		if (setTokenInvalid == "OK") {
			res.status(200).json(returnResponse(true, {}, "Logout realizado com sucesso!"));
		} else {
			throw new Error("Não foi possível realizar o logout. Tente Novamente!");
		}

	} catch (error: any) {
		res.status(500).json(error);
	}
}

async function editUser(req: Request, res: Response) {
	const id: string = req.params.id;
	const userPrevEdit: UserEdit = req.body;

	if (!userPrevEdit.name && !userPrevEdit.birthdate) {
		return res.status(400).json(returnResponse(false, {}, "É necessário os dados que deseja alterar!"));
	}

	const userExist: boolean = await verifyUserExists(id);

	if (userExist) {
		const userEdited: UserEdit = await prisma.user.update({
			where: {
				id: Number(id)
			},
			data: userPrevEdit,
			select: {
				id: true,
				name: true,
				birthdate: true,
				nif: true,
				email: true,
				type_user: true
			}
		});
		return res
			.status(200)
			.json(returnResponse(true, userEdited, "Usuário editado com sucesso!"));
	} else {
		return res
			.status(404)
			.json(returnResponse(false, {}, "Usuário não encontrado!"));
	}
}

async function verifyUserExists(id_user: string): Promise<boolean> {
	const userExist: User | null = await prisma.user.findUnique({
		where: {
			id: Number(id_user)
		},
	});

	return userExist ? true : false;

}

export default {
	registerUser,
	loginUser,
	logoutUser,
	editUser
};



