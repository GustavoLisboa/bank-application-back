//importação de biblioteca
import { Router } from "express";
import authController from "../controllers/authController";
import { checkToken } from "../middlewares/authMiddleware";

//importação de controllers

export const router = Router();


router.post("/auth/register", authController.registerUser);
router.post("/auth/login", authController.loginUser);
router.post("/auth/logout", checkToken, authController.logoutUser);
router.put("/auth/user/:id", checkToken, authController.editUser);



