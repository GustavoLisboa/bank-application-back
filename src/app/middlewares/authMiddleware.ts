import { NextFunction, Request, Response } from "express";
import { getRedis } from "../utils/redisClient";
import { returnResponse } from "../utils/response";
import { getHeaderAuthorization } from "../utils/getHeaderAuthorization";

export async function checkToken(request: Request, response: Response, next: NextFunction) {
	const token = await getHeaderAuthorization(request);
	if (!token) {
		return response.status(401).json(returnResponse(false, {}, "Acesso negado"));
	}
	try {
		const tokenValid = await getRedis(`token:${token}`);
		if(tokenValid == "invalid"){
			throw new Error("O token utilizado, está inválido. Faça login novamente");
		} else{
			next();
		}
	} catch (error: any) {
		response.status(400).json(returnResponse(false, {}, error.message));
	}
}