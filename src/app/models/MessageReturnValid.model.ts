export interface MessageReturnValid {
	message: string
	valid: boolean
}