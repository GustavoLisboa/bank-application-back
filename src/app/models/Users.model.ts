export type User = {
	name: string
	email: string
	nif: string
	birthdate: string
	type_user: number
	password: string
	id: number
}

export type UserEdit = {
	name: string
	birthdate: string
}