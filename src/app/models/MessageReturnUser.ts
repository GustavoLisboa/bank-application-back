import { User } from "./Users.model";

export interface MessageReturnUserJson {
	user: User | null
	valid: boolean
	message: string
	status: number
}

export interface LoginJson{
	user: User
	token: string
}